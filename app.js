const express=require("express")
const app=express()
function normalizePort(val) {
    var port = parseInt(val, 10);
  
    if (isNaN(port)) {
      // named pipe
      return val;
    }
  
    if (port >= 0) {
      // port number
      return port;
    }
  
    return false;
  }
const port=normalizePort(process.env.PORT || 3000)

app.get('/', (req,res)=>{res.sendFile('D:/Worldline Training/assessment1/index.html')})

app.listen(port, ()=>console.log("The NodeJS application is running successfully."))